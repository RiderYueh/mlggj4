﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMenu : MonoBehaviour
{
    public void OnClickStage(GameObject go)
    {
        string sceneName = "";
        if(go.name == "1")
        {
            sceneName = "Stage1";
        }

        SceneManager.LoadScene(sceneName);
    }
}
