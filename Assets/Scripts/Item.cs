﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    private Vector3 OriPos;
    int OriLayer;
    GameObject OriParent;
    Vector3 oriScale;
    // Start is called before the first frame update
    void Start()
    {
        OriPos = this.transform.position;
        OriLayer = this.gameObject.layer;
        OriParent = transform.parent.gameObject;
        oriScale = gameObject.transform.localScale;
    }

    public void Dragging()
    {
        gameObject.layer = 0;
        gameObject.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }
    
    public void CallReset()
    {
        transform.position = OriPos;
        gameObject.layer = OriLayer;
        transform.SetParent(OriParent.transform);
        gameObject.transform.localScale = oriScale;
    }

    public void CallChangePlace(Transform target)
    {
        transform.position = target.position;
        gameObject.layer = OriLayer;
        transform.SetParent(OriParent.transform);
        gameObject.transform.localScale = oriScale;
    }
}
