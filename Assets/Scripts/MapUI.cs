﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapUI : MonoBehaviour
{
    public GameObject MainUI;
    public GameObject EndUI;
    public Text mainUIText;
    public Text endUIText;

    public AudioSource m_audio;
    public AudioClip m_audioClip;


    private void Start()
    {
        if(MainManager.instance.stageNow == 1)
        {
            MainUI.SetActive(true);
        }
        else
            MainUI.SetActive(false);

        if (MainManager.instance.stageNow >= 5)
            ShowEndUI();
    }


    public void OnClickOK()
    {
        MainUI.SetActive(false);
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void ShowEndUI()
    {
        m_audio.clip = m_audioClip;
        m_audio.Play();

        EndUI.SetActive(true);

        string endText = "";
        float hitRate = System.Convert.ToSingle(MainManager.instance.HitCount) / System.Convert.ToSingle(MainManager.instance.ShootCount) * 100;

        int stageTimerAndHitRate = System.Convert.ToInt32(MainManager.instance.Timer / hitRate);

        endText = "恭喜過關!!!!!!" + "\n";
        endText += "通關時間 : " + MainManager.instance.Timer.ToString("f0") +"秒"+ "\n";
        endText += "泡泡命中率 : " + hitRate.ToString("f0") + " % " + "\n";
        endText += "被擊中次數 : " + MainManager.instance.beHitCount + "\n";
        
        if(hitRate > 40 && MainManager.instance.beHitCount < 3)
        {
            endText += "全球遊戲排名第 : 1 " + "名";
        }
        else
        {
            int rank = Random.Range( 20 + stageTimerAndHitRate, 50 + stageTimerAndHitRate);
            endText += "全球排名第 : " + rank.ToString()  + "名";
        }

        endUIText.text = endText;
    }

    public void OnExitGame()
    {
        Application.Quit();
    }
}
