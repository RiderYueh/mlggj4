﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainManager : MonoBehaviour
{
    static public MainManager instance;

    //音效特效先放好 撥很快
    public List<GameObject> particlePrefabList;
    List<string> particlePrefabStringList;

    public int ShootCount = 0;
    public int HitCount = 0;
    public int stageNow = 0;

    public float Timer;
    public int beHitCount = 0;

    public string testString;
    public Quaternion m_Quaternion;

    private void Awake()
    {
        if(instance == null)
            instance = this;

#if UNITY_STANDALONE
        Screen.SetResolution(450, 800, false);
        Screen.fullScreen = false;
#endif

        DontDestroyOnLoad(gameObject);
        stageNow = 1;
        Timer = 0;
        Cursor.lockState = CursorLockMode.Confined;




        particlePrefabStringList = new List<string>();
        foreach (GameObject go in particlePrefabList)
        {
            particlePrefabStringList.Add(go.name);
        }

        ChangeScene("Map");
    }

    [RuntimeInitializeOnLoadMethod]
    static void Initialize()
    {
        SceneManager.LoadScene("Awake");
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }


    public void CreatParticle(Vector3 thePosition, string theName, float destroyTime = 3)
    {
        if (particlePrefabStringList.Contains(theName))
        {
            int index = particlePrefabStringList.IndexOf(theName);
            GameObject go = Instantiate(particlePrefabList[index], thePosition, transform.rotation) as GameObject;
            Destroy(go, destroyTime);
        }
    }

    public void CreatParticleWithTransform(Transform _transform, string theName, float destroyTime = 3)
    {
        if (particlePrefabStringList.Contains(theName))
        {
            int index = particlePrefabStringList.IndexOf(theName);
            GameObject go = Instantiate(particlePrefabList[index], _transform.position, _transform.rotation) as GameObject;
            Destroy(go, destroyTime);
        }
    }

}
