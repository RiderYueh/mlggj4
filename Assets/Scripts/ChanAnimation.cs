﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChanAnimation : MonoBehaviour
{
    public Animator m_Animator;
    public GameObject sheild;


    private void Update()
    {
        if (Input.GetKey("w"))
        {
            m_Animator.SetBool("IsDown", false);
        }
        if (Input.GetKey("s"))
        {
            m_Animator.SetBool("IsDown", true);
        }

#if UNITY_ANDROID || UNITY_IOS
        if (MainManager.instance.m_Quaternion.x > 0)
        {
            m_Animator.SetBool("IsDown", false);
            if (StageManager.instance != null)
            {
                StageManager.instance.m_CountClick.isSheild = false;
                sheild.SetActive(false);
            }
        }
        else
        {
            m_Animator.SetBool("IsDown", true);
            if (StageManager.instance != null)
            {
                StageManager.instance.m_CountClick.isSheild = true;
                sheild.SetActive(true);
            }
        }
#endif


    }
}
