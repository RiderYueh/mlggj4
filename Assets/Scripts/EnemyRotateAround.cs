﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRotateAround : MonoBehaviour
{
    public float speed = 20;
    Camera mainCamera;
    public int m_HP;
    public Material oriMat;
    public Material hitMat;
    public SkinnedMeshRenderer smr;
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        isMove = true;
    }
    bool isMove;
    float lookTimer;
    float moveSpeed = 3;
    float shootTimer = 5;
    // Update is called once per frame
    void Update()
    {
        if(isMove)
        {
            lookTimer -= Time.deltaTime;
            shootTimer -= Time.deltaTime;

            Vector3 rotatePos = new Vector3(mainCamera.transform.position.x , transform.position.y , mainCamera.transform.position.z);

            //transform.GetChild(0).LookAt(rotatePos);

            transform.RotateAround(rotatePos, new Vector3(0, 1, 0), speed * Time.deltaTime);

            if(lookTimer <=0)
            {
                transform.LookAt(rotatePos);
                lookTimer = Random.Range(3, 6);
                moveSpeed = Random.Range(1f, 4f);
                speed = Random.Range(20f, 30f);
            }

            if(shootTimer <= 0)
            {
                OnShootBullet();
                shootTimer = Random.Range(3f, 8f);
            }
            
            transform.position += transform.forward * Time.deltaTime * moveSpeed;


        }

        redTimer -= Time.deltaTime;
        if(isRedNow && redTimer < 0)
        {
            ChangeColorRed(false);
            
        }

        if(m_HP <= 0)
        {
            MainManager.instance.CreatParticle(transform.position, "14_bossDie", 1);
            Debug.Log("Boss Die");
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Bullet")
        {
            collision.gameObject.tag = "BulletEnd";
            ChangeColorRed(true);
        }
    }

    float redTimer;
    bool isRedNow;
    void ChangeColorRed(bool isRed)
    {
        if(isRed)
        {
            if (isRedNow) return;
            MainManager.instance.HitCount += 1;
            MainManager.instance.CreatParticleWithTransform(transform, "13_hitBoss", 1);
            smr.material = hitMat;
            redTimer = 0.3f;
            isRedNow = true;
            m_HP -= 10;
        }
        else
        {
            smr.material = oriMat;
            isRedNow = false;
        }
    }

    void OnShootBullet()
    {
        MainManager.instance.CreatParticleWithTransform(transform, "4_monsterShoot", 1);
        GameObject go = Instantiate(StageManager.instance.BulletBoss, transform.position - new Vector3(0,1,0), transform.rotation) as GameObject;
        go.GetComponent<BulletFly>().isBulletBoss = true;


    }
}
