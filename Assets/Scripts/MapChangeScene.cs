﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapChangeScene : MonoBehaviour
{
    bool isOpen;
    int stage = 0;
    private void Start()
    {
        if(gameObject.name == "RealStage1")
        {
            stage = 1;
        }
        if (gameObject.name == "RealStage2")
        {
            stage = 2;
        }
        if (gameObject.name == "RealStage3")
        {
            stage = 3;
        }
        if (gameObject.name == "RealStage4")
        {
            stage = 4;
        }

        if(MainManager.instance.stageNow == stage)
        {
            isOpen = true;
        }

        if(isOpen)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isOpen) return;
        if (other.tag == "Player")
        {
            MainManager.instance.ChangeScene(transform.name);
        }
    }
}
