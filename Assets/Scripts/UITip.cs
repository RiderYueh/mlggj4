﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITip : MonoBehaviour
{
    [SerializeField]
    List<Sprite> Images = new List<Sprite>();
    [SerializeField]
    Image TipImage;

    public Text clearBossText;

    private void Start()
    {
        
    }

    public void ActiveUI()
    {
        if (gameObject.activeInHierarchy == false)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void ChangeTip(string id)
    {
        foreach (var image in Images)
        {
            if (image.name == id)
                TipImage.sprite = image;
        }
    }

    public void ShowClearBoss(string contant)
    {
        clearBossText.transform.parent.gameObject.SetActive(true);
        clearBossText.text = contant;
    }

    public void OnLeave()
    {
        StageManager.instance.OnLeaveMap();
    }


}
