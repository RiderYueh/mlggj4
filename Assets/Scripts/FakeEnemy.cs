﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeEnemy : MonoBehaviour
{
    public float speed = 20;
    public GameObject fishObject;

    void Start()
    {

    }

    bool isStart;
    bool isTouchPlayer;
    void Update()
    {
        if (isStart) return;
        if(StageManager.instance.isShowFakeBoss)
        {
            fishObject.SetActive(true);
            transform.position += transform.forward * Time.deltaTime * speed;
            Transform playerPos = StageManager.instance.m_CountClick.player.transform;
            if(isTouchPlayer)
            {
                playerPos = StageManager.instance.bossPool.transform.GetChild(0);
            }

            transform.LookAt(playerPos);

            if(Vector3.Distance(transform.position , playerPos.position) < 1 )
            {
                if(isTouchPlayer)
                {
                    StageManager.instance.ShowBoss2();
                    Destroy(gameObject);
                }

                isTouchPlayer = true;
                
            }
        }
        

    }
}
