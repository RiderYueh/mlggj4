﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    public static StageManager instance;

    public GameObject bossPool;

    public CameraControl m_CameraControl;
    public CountClick m_CountClick;

    public GameObject BulletBoss;

    public int Stage;

    public AudioSource m_AudioSource;
    public AudioClip m_Bossm_AudioSource;

    public UITip m_UITip;
    public bool isStage2Plug;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        m_CameraControl.Finding();
        m_AudioSource = GetComponent<AudioSource>();
    }

    public bool isShowFakeBoss = false;

    public void ShowBoss()
    {
        isShowFakeBoss = true;
        MainManager.instance.CreatParticleWithTransform(transform, "3_monsterLaugh", 1);
    }

    bool isShowBoss2;

    public void ShowBoss2()
    {
        if (isShowBoss2) return;
        isShowBoss2 = true;
        m_CameraControl.BossFight();
        m_AudioSource.clip = m_Bossm_AudioSource;
        m_AudioSource.Play();

        foreach (Transform child in bossPool.transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    float bossDieTimer = 2;
    bool isCheck;
    
    private void Update()
    {
        MainManager.instance.Timer += Time.deltaTime;


        if (bossPool.transform.childCount == 0)
        {
            bossDieTimer -= Time.deltaTime;
            if(bossDieTimer <= 0 && !isCheck)
            {
                isCheck = true;

                if(Stage == 1)
                {
                    m_UITip.ShowClearBoss("不知道你怎麼發現我的...\n但不重要，我先走一步，掰掰。");
                }
                if (Stage == 2)
                {
                    if(isStage2Plug)
                    {
                        m_UITip.ShowClearBoss("可惡，竟然敢動我的塞子，\n下次不會饒過妳的。掰掰。");
                    }
                    else
                    {
                        m_UITip.ShowClearBoss("可惡，連下棋都輸給妳，\n下次妳不會這麼好運的。掰掰。");
                    }
                    
                }
                if (Stage == 3)
                {
                    m_UITip.ShowClearBoss("妳...怎麼知道樹上會有洞??");
                }
                if (Stage == 4)
                {
                    m_UITip.ShowClearBoss("我輸了...不敢再來台北了...");
                }


            }
        }
    }

    public void OnLeaveMap()
    {
        MainManager.instance.stageNow += 1;
        MainManager.instance.ChangeScene("Map");
    }

}
