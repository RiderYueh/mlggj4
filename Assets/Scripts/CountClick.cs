﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountClick : MonoBehaviour
{
    Camera mainCamera;
    public GameObject changeStageParent;
    public GameObject player;
    public GameObject playerhand;
    public bool IsMove;
    public bool isSheild;
    private void Start()
    {
        mainCamera = Camera.main;
    }
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if(IsMove)
            MoveTo();
        }
        //CheckChangeScene();
        //CheckSheild();
    }

    float changeSceneTimer = 0;

    void MoveTo()
    {
        Vector3 gotoPos = mainCamera.transform.forward;
        transform.position += new Vector3(gotoPos.x , 0 , gotoPos.z) * Time.deltaTime * 5;
    }

    void CheckChangeScene()
    {
        if (!IsMove) return;
        foreach (Transform child in changeStageParent.transform)
        {
            if (Vector3.Distance(child.position, player.transform.position) < 1)
            {
                changeSceneTimer += Time.deltaTime;
                if (changeSceneTimer >= 0.1f)
                {
                    MainManager.instance.ChangeScene(child.name);
                }
                return;
            }
        }
        changeSceneTimer = 0;
    }

    float sheildTimer;

    void CheckSheild()
    {
        sheildTimer -= Time.deltaTime;
        if (Input.GetKey(KeyCode.Space))
        {
            Debug.Log("產生防護盾");
            sheildTimer = 1;
            isSheild = true;
        }
        if(sheildTimer <= 0)
        {
            
            if(isSheild)
            {
                Debug.Log("防護盾沒了");
                isSheild = false;
            }
            

        }
    }
}
