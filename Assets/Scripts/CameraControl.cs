﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CameraControl : MonoBehaviour
{

    Camera maincamera;
    [SerializeField]
    GameObject clearMessage;
    [SerializeField]
    GameObject Bullet;
    [SerializeField]
    CameraMode currentMode = CameraMode.Finding;
    [SerializeField]
    float AimTime = 3.0f;
    [SerializeField]
    public GameObject playerhand;
    string takeName = "";

    public Text debugText;

    enum CameraMode
    {
        Shooting,
        Finding,
    }

    private float rotY = 0.0f;
    private float rotX = 0.0f;

    public void BossFight()
    {
        currentMode = CameraMode.Shooting;
    }

    public void Finding()
    {
        currentMode = CameraMode.Finding;
    }

    private void Start()
    {
        Input.gyro.enabled = true;
        maincamera = Camera.main;
        correctionQuaternion = Quaternion.Euler(90f, 0f, 0f);
        Cursor.lockState = CursorLockMode.Confined;
    }

    void Update()
    {
# if UNITY_IPHONE || UNITY_ANDROID
        RotateGyro();
#else
        RotateByKey();
#endif

        switch (currentMode)
        {
            case CameraMode.Finding:
                Find();
                break;
            case CameraMode.Shooting:
                Shoot();
                break;
        }
    }

    private void Shoot()
    {
        if(Input.GetMouseButtonDown(0))
        {
            MainManager.instance.ShootCount += 1;
            MainManager.instance.CreatParticle(transform.position , "1_shoot");
            MainManager.instance.CreatParticleWithTransform(playerhand.transform, "BubbleSoft", 1);
            Instantiate(Bullet, maincamera.transform.position, maincamera.transform.rotation);
        }
    }
    Item dragItem = null;
    int counter = 0;
    bool isClear;
    private void Find()
    {
        if (isClear) return;
        if(SceneManager.GetActiveScene().name == "Map")
        {
            return;
        }

        if(StageManager.instance.Stage == 4)
        {
            StageManager.instance.ShowBoss2();
        }

        if (Input.GetMouseButtonDown(0))
        {
            MainManager.instance.CreatParticleWithTransform(transform, "12_click", 1);
        }

        int LayerSMask = LayerMask.GetMask("Item");
        Ray ray = new Ray(maincamera.transform.position, maincamera.transform.TransformDirection(Vector3.forward));
        RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerSMask))
        {
            if (hit.transform.tag == "Item")
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if(StageManager.instance.Stage == 1)
                    {
                        StageManager.instance.ShowBoss();
                        isClear = true;
                        return;
                    }
                    takeName = hit.transform.name;
                    if (takeName == "Clear")
                    {
                        return;
                    }

                    if (StageManager.instance.Stage == 3)
                    {
                        MainManager.instance.CreatParticleWithTransform(playerhand.transform, "11_attackTree", 1);
                        counter += 1;

                        if(counter >= 6)
                        {
                            isClear = true;
                            StageManager.instance.ShowBoss();
                        }
                        
                        return;
                    }


                    //Debug.Log(hit.transform.name);
                    if (hit.transform.parent != playerhand.transform)
                    {
                        MainManager.instance.CreatParticleWithTransform(transform, "6_pickUp", 1f);
                        dragItem = hit.transform.GetComponent<Item>();
                        hit.transform.GetComponent<Item>().Dragging();
                        hit.transform.position = playerhand.transform.position;
                        hit.transform.SetParent(playerhand.transform);
                    }
                }
                //放開時
                if (Input.GetMouseButtonUp(0))
                {
                    if (takeName == "Key")
                    {
                        if (StageManager.instance.Stage == 2)
                        {
                            if (hit.transform.gameObject.name == "Clear")//放開所指的物件也是解迷物件
                            {
                                MainManager.instance.CreatParticleWithTransform(transform, "9_bingo", 2f);
                                StageManager.instance.ShowBoss();
                                dragItem.CallChangePlace(hit.transform);
                                isClear = true;
                                return;
                            }
                        }
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (dragItem != null)
            {
                if (StageManager.instance.Stage == 2)
                {
                    if (dragItem.gameObject.name == "Plug")
                    {
                        MainManager.instance.CreatParticleWithTransform(transform, "9_bingo", 2f);
                        StageManager.instance.ShowBoss();
                        dragItem.CallReset();
                        dragItem = null;
                        isClear = true;
                        StageManager.instance.isStage2Plug = true;
                        return;
                    }
                }

                dragItem.CallReset();
                dragItem = null;
                MainManager.instance.CreatParticleWithTransform(transform, "5_wrongAnswer", 1);
            }
        }
    }


    private Quaternion correctionQuaternion;
    Quaternion gyroQuaternion;
    Quaternion calculatedRotation;
    void RotateGyro()
    {
        gyroQuaternion = GyroToUnity(Input.gyro.attitude);
        calculatedRotation = correctionQuaternion * gyroQuaternion;
        maincamera.transform.parent.rotation = calculatedRotation;
        MainManager.instance.m_Quaternion = calculatedRotation;
        //debugText.text = calculatedRotation.ToString();
    }

    void RotateByKey()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        rotY = rotY + mouseX * 200 * Time.deltaTime;
        rotX = rotX + mouseY * 200 * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -80, 80);

        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        maincamera.transform.parent.rotation = localRotation;
        //MainManager.instance.testString = localRotation.ToString();
    }

    private static Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

}
