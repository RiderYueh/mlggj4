﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFly : MonoBehaviour
{
    public bool isBulletBoss;
    Rigidbody rigid;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();

        if (isBulletBoss)
        {
            transform.LookAt(StageManager.instance.m_CountClick.player.transform);
            speed = 5;
        }

        Destroy(gameObject, 5.0f);

    }

    // Update is called once per frame
    void Update()
    {
        rigid.velocity = transform.forward * speed;

        if (isBulletBoss)
        {
            transform.LookAt(StageManager.instance.m_CountClick.player.transform);
        }

        // Destroy the bullet after 2 seconds
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!isBulletBoss) return;
            if(StageManager.instance.m_CountClick.isSheild)
            {
                MainManager.instance.CreatParticle(transform.position, "8_defenseHit");
                Destroy(gameObject);
            }
            else
            {
                MainManager.instance.beHitCount += 1;
                MainManager.instance.CreatParticle(transform.position, "2_hurt");
                Destroy(gameObject);
            }

            
        }
    }
}
